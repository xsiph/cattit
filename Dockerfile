FROM clux/muslrust AS builder
WORKDIR /volume
COPY . .
RUN cargo build --release

FROM jrottenberg/ffmpeg:4.1-scratch
COPY --from=builder /volume/target/x86_64-unknown-linux-musl/release/cattit .
ENTRYPOINT [ "/cattit" ]
