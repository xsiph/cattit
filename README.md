```text
cattit 0.1.1
Chris Dawkins
--------------------------------------------------------------------
Cattit is used to concatenate all video files in a directory into one.

FFMPEG is required to run cattit.

Files are output into the directory of where the programs is executed from.
--------------------------------------------------------------------

USAGE:
    cattit [FLAGS] [OPTIONS] --input <input>

FLAGS:
    -d, --delete     Delete original files when done
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v, --verbose    Show ffmpeg output

OPTIONS:
    -i, --input <input>    Sets the input directory
    -t, --title <title>    Sets title of new video
```

## Build Instructions
```bash
cargo build --release && cp ./target/release/cattit ~/.local/bin/
```

## Docker Instructions
Build:
```bash
docker build -t xsiph/cattit .
```
Run:
```bash
docker run -v "$PWD:$PWD" -w "$PWD" xsiph/cattit
```
